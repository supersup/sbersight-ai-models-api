<p align="center">
  <img src="./docs/logo.jpeg" width="320" alt="Fusionbrain Logo" />
</p>

<h1 align="center">FusionBrain</h1>

<p align="center">FusionBrain - spot of sber ai models.</p>
<p align="center">
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
  <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
  <a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
  <a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
  <a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
  <a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
  <a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>

# Database

## Start environment in docker

```bash
# create network
$ docker network create mdb

# run mongo image
$ docker run -d \
  -p 27017:27017 \
  --network mdb \
  -e MONGO_INITDB_ROOT_USERNAME=sberaimodels \
  -e MONGO_INITDB_ROOT_PASSWORD=1q2w3e4r \
  --name sberai-mongo \
  mongo

# run rabbitmq image
$ docker run -d \
  -p 5672:5672
  -p 15672:15672
  --network mdb \
  --name sberai-rmq \
  rabbitmq:3

# run elasticsearch image
$ docker run -d \
  --name elasticsearch \
  --net somenetwork \
  -p 9200:9200 \
  -p 9300:9300 \
  -e "discovery.type=single-node" \
  elasticsearch:8.7.0
```

***Optional arguments***

- -e MONGO_INITDB_ROOT_USERNAME={DB_USER_NAME}
- -e MONGO_INITDB_ROOT_PASSWORD={DB_PASSWORD}

# Start service local

## Install dependencies

```bash
npm install
```

## Start server

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# debug mode
$ npm run start:debug

# production mode
$ npm run start:prod
```

# Run app in docker

```bash
# create network
$ docker netword create mdb

# build image
$ npm run docker:build

# run image
$ npm run docker:run

# create network
$ docker netword connect mdb ai-models-api
```

# Run app in docker-compose

```bash
# create network
$ npm run compose:build

# build image
$ npm run compose:run
```

# Build/Start docker image

```bash
# Build docker image
docker build -f ./docker/Dockerfile -t ai-models-api .

# Start docker container from image
docker run --rm -it -p 3000:8080 ai-models-api
```

# Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Info

## Connect to container

```bash
docker exec -it {container_name} bash
```
