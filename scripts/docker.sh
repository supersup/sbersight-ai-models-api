#!/bin/bash

name=ai-models-api;
version=latest;

while getopts n:t:v: flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        t) type=${OPTARG};;
        v) version=${OPTARG};;
    esac
done

normalizedName=$(echo $name | sed "s/\//_/g")

if [ "$type" = "build" ]; then
    build_args=""

    for i in `cat ./docker/.env`; do
        build_args+="--build-arg $i ";
    done;

    docker build \
        -f ./docker/Dockerfile \
        -t "$name:$version" \
        $build_args \
        .
elif [ "$type" = "run" ]; then
    docker run \
        -d \
        --rm \
        --name "$(echo $normalizedName)_container" \
        -p "8080:8080" \
        "$name:$version"
elif [ "$type" = "kill" ]; then
    docker stop "$(echo $normalizedName)_container"
    docker rm "$(echo $normalizedName)_container"
    docker rmi "$name:$version"
fi

exit 0;