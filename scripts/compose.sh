#!/bin/bash

while getopts n:t:v: flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        t) type=${OPTARG};;
        v) version=${OPTARG};;
    esac
done

if [ "$type" = "build" ]; then
    docker compose \
        -f ./docker/docker-compose.yml \
        --env-file ./docker/.env \
        --project-directory . \
        build
elif [ "$type" = "up" ]; then
    docker compose \
        -f ./docker/docker-compose.yml \
        --env-file ./docker/.env \
        --project-directory . \
        up -d
elif [ "$type" = "kill" ]; then
    docker compose \
        -f ./docker/docker-compose.yml \
        --env-file ./docker/.env \
        --project-directory . \
        down
fi

exit 0