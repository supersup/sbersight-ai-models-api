import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';
import { DigitalpetrModule } from './modules/digitalpetr/digitalpetr.module';
import { HandwrittenModule } from './modules/handwritten/handwritten.module';
import { FaceswapModule } from './modules/faceswap/faceswap.module';
// import { StyletransferModule } from './modules/styletransfer/styletransfer.module';
import { Text2ImageModule } from './modules/text2image/text2image.module';
import { HealthModule } from './modules/health/health.module';
import Configuration from './config/Configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [Configuration.configFactory],
    }),
    MongooseModule.forRoot(process.env.DB_HOST, {
      connectionName: process.env.DP_DB_NAME,
      dbName: process.env.DP_DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      authSource: process.env.DP_DB_AUTH_SOURCE,
    }),
    MongooseModule.forRoot(process.env.DB_HOST, {
      connectionName: process.env.HW_DB_NAME,
      dbName: process.env.HW_DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      authSource: process.env.HW_DB_AUTH_SOURCE,
    }),
    MongooseModule.forRoot(process.env.DB_HOST, {
      connectionName: process.env.FS_DB_NAME,
      dbName: process.env.FS_DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      authSource: process.env.FS_DB_AUTH_SOURCE,
    }),
    MongooseModule.forRoot(process.env.DB_HOST, {
      connectionName: process.env.ST_DB_NAME,
      dbName: process.env.ST_DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      authSource: process.env.ST_DB_AUTH_SOURCE,
    }),
    MongooseModule.forRoot(process.env.DB_HOST, {
      connectionName: process.env.T2I_DB_NAME,
      dbName: process.env.T2I_DB_NAME,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      authSource: process.env.T2I_DB_AUTH_SOURCE,
    }),
    ScheduleModule.forRoot(),
    HealthModule,
    Text2ImageModule,
    FaceswapModule,
    DigitalpetrModule,
    HandwrittenModule,
    // StyletransferModule,
    PrometheusModule.register(),
  ],
})
export class AppModule {}
