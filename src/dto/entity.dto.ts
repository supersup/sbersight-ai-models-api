import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsDate,
  IsDefined,
  IsEnum,
  IsString,
} from 'class-validator';
import { Status } from './status.dto';

export class EntityDto {
  @IsString()
  _id: string;

  @IsString()
  bunchId: string;

  @IsEnum(Status)
  status: Status;

  @IsBoolean()
  error: boolean;

  @IsDefined()
  response: any;

  @IsDate()
  createdAt?: string;

  @IsDefined()
  params?: any[];

  // TODO: HASH is temporary solution, its will be removed later
  @IsString()
  hash: string;
}

export class PushToQueueReqDto {
  @ApiProperty({
    description: 'Массив элементов на обработку',
    type: [Object],
    required: true,
  })
  @IsArray()
  items: any[];
}

export class PushToQueueResDto {
  @ApiProperty({ description: 'Уникальный идентификатор пакета' })
  bunchId: string;
}

export class EntityReqDto {
  @ApiProperty({ description: 'Id пакета' })
  @IsString()
  entityId: string;
}
