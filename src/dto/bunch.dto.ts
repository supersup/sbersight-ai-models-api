import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsDate, IsEnum, IsString } from 'class-validator';
import { Status } from './status.dto';

export class BunchDto {
  @ApiProperty({ description: 'Id пакета' })
  @IsString()
  _id: string;

  @ApiProperty({ description: 'Статус' })
  @IsEnum(Status)
  status: Status;

  @ApiProperty({ description: 'Дата создания' })
  @IsDate()
  createdAt: Date;
}

export class BunchStatusReqDto {
  @ApiProperty({ description: 'Id пакета' })
  @IsString()
  bunchId: string;
}

export class BunchStatusResDto extends PickType(BunchDto, [
  'status',
] as const) {}
