export interface DigitalPetr {
  digitalpetr: {
    aiModelHost: string;
    authUrl: string;
    xApiKey: string;
    xWorkspaceId: string;
    deployId: string;
    xEmail: string;
    xPassword: string;
    database: {
      host: string;
      name: string;
      user: string;
      password: string;
    };
  };
}

export interface Handwritten {
  handwritten: {
    aiModelHost: string;
    authUrl: string;
    xApiKey: string;
    xWorkspaceId: string;
    deployId: string;
    xEmail: string;
    xPassword: string;
    database: {
      host: string;
      name: string;
      user: string;
      password: string;
    };
  };
}

export interface Faceswap {
  faceswap: {
    aiModelHost: string;
    authUrl: string;
    xApiKey: string;
    xWorkspaceId: string;
    deployId: string;
    xEmail: string;
    xPassword: string;
    database: {
      host: string;
      name: string;
      user: string;
      password: string;
    };
  };
}

export interface Styletransfer {
  styletransfer: {
    authUrl: string;
    database: {
      host: string;
      name: string;
      user: string;
      password: string;
    };
    aiModelHosts: {
      classic: string;
      fast: string;
    };
  };
}

export interface Text2image {
  text2image: {
    aiModelHost: string;
    authUrl: string;
    xApiKey: string;
    xWorkspaceId: string;
    deployId: string;
    database: {
      host: string;
      name: string;
      user: string;
      password: string;
    };
  };
}

export interface Config
  extends Faceswap,
    Styletransfer,
    DigitalPetr,
    Handwritten,
    Text2image {
  port: number;
}

class Configuration {
  private static config: Config;

  static configFactory(): Config {
    return {
      port: parseInt(process.env.PORT, 10) || 3000,
      digitalpetr: {
        database: {
          host: process.env.DB_HOST,
          name: process.env.DP_DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        aiModelHost: process.env.DP_AI_MODEL_HOST,
        authUrl: process.env.DP_AUTH_URL,
        xApiKey: process.env.DP_X_API_KEY,
        xWorkspaceId: process.env.DP_X_WORKSPACE_ID,
        deployId: process.env.DP_DEPLOY_ID,
        xEmail: process.env.DP_X_EMAIL,
        xPassword: process.env.DP_X_PASSWORD,
      },
      handwritten: {
        database: {
          host: process.env.DB_HOST,
          name: process.env.DP_DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        aiModelHost: process.env.HW_AI_MODEL_HOST,
        authUrl: process.env.HW_AUTH_URL,
        xApiKey: process.env.HW_X_API_KEY,
        xWorkspaceId: process.env.HW_X_WORKSPACE_ID,
        deployId: process.env.HW_DEPLOY_ID,
        xPassword: process.env.HW_X_PASSWORD,
        xEmail: process.env.HW_X_EMAIL,
      },
      faceswap: {
        database: {
          host: process.env.DB_HOST,
          name: process.env.FS_DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        aiModelHost: process.env.FS_AI_MODEL_HOST,
        authUrl: process.env.FS_AUTH_URL,
        xWorkspaceId: process.env.FS_X_WORKSPACE_ID,
        xApiKey: process.env.FS_X_API_KEY,
        deployId: process.env.FS_DEPLOY_ID,
        xEmail: process.env.FS_X_EMAIL,
        xPassword: process.env.FS_X_PASSWORD,
      },
      styletransfer: {
        database: {
          host: process.env.DB_HOST,
          name: process.env.ST_DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        aiModelHosts: {
          classic: process.env.ST_CLASSIC_AI_MODEL_HOST,
          fast: process.env.ST_FAST_AI_MODEL_HOST,
        },
        authUrl: process.env.ST_AUTH_URL,
      },
      text2image: {
        database: {
          host: process.env.DB_HOST,
          name: process.env.T2I_DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        aiModelHost: process.env.T2I_AI_MODEL_HOST,
        authUrl: process.env.T2I_AUTH_URL,
        xApiKey: process.env.T2I_X_API_KEY,
        xWorkspaceId: process.env.T2I_X_WORKSPACE_ID,
        deployId: process.env.T2I_DEPLOY_ID,
      },
    };
  }

  get getConfig() {
    if (!Configuration.config) {
      Configuration.config = Configuration.configFactory();
    }

    return Configuration.config;
  }
}

export default Configuration;
