import { gzip, unzip, constants } from 'zlib';

export async function compressString(source: string) {
  return new Promise((resolve, reject) => {
    console.log('ORIGINAL: ', Buffer.byteLength(source, 'base64'));
    gzip(source, { level: constants.Z_BEST_COMPRESSION }, (err, res) => {
      if (err) {
        reject(res);
      }

      console.log(
        'COMPRESSED: ',
        Buffer.byteLength(res.toString('base64'), 'base64'),
      );
      resolve(res);
    });
  });
}

export async function decompressString(source: string) {
  return new Promise((resolve, reject) => {
    unzip(source, { level: constants.Z_BEST_COMPRESSION }, (err, res) => {
      if (err) {
        reject(res);
      }

      resolve(res);
    });
  });
}
