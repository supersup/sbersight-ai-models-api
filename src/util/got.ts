import got from 'got';
import Configuration from 'src/config/Configuration';

type AuthRes = {
  token: { access_token: string };
};

export const getHooks = (service: string) => ({
  hooks: {
    beforeRequest: [
      async (options) => {
        try {
          const config = new Configuration().getConfig;

          const authUrl = config[service]?.authUrl;
          const email = config[service]?.xEmail;
          const password = config[service]?.xPassword;
          const xApiKey = config[service]?.xApiKey;
          const xWorkspaceId = config[service]?.xWorkspaceId;

          if (authUrl) {
            const authCredentials = {
              email,
              password,
            };

            const { token } = await got
              .post(authUrl, {
                json: authCredentials,
                headers: {
                  'x-api-key': xApiKey,
                },
              })
              .json<AuthRes>();

            options.headers.authorization = token.access_token;
          }

          options.headers['x-api-key'] = xApiKey;
          options.headers['x-workspace-id'] = xWorkspaceId;
        } catch (e) {
          console.error(e);
        }
      },
    ],
  },
});

const instance = got.extend();

export default instance;
