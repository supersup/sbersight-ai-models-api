import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import * as compression from 'compression';
import * as bodyParser from 'body-parser';
import { Config } from 'src/config/Configuration';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.setGlobalPrefix('/api/v1/models', { exclude: ['metrics'] });
  app.use(compression());
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.getHttpAdapter().getInstance().set('etag', false);

  const config = new DocumentBuilder()
    .setTitle('SberSight AI models API')
    .setDescription('AI models API endpoints')
    .addTag('styletransfer')
    .addTag('digital-petr')
    .addTag('handwritten')
    .addTag('faceswap')
    .addTag('text2image')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/v1/models/swagger', app, document);

  const configService = app.get<ConfigService<Config>>(ConfigService);
  const port = configService.get('port');

  await app.listen(port);
}

bootstrap();
