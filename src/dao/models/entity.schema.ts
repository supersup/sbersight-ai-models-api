import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export interface EntityDocument extends Document, Entity {}

@Schema()
export class Entity {
  @Prop({ type: String, required: true, index: true })
  bunchId: string;

  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: string;

  @Prop({ type: MongooseSchema.Types.Date })
  updatedAt?: string | number;

  @Prop({ type: [MongooseSchema.Types.Mixed], default: null })
  error?: string[];

  @Prop({ type: [MongooseSchema.Types.Mixed], default: null })
  response?: any[];

  @Prop({ type: MongooseSchema.Types.Mixed, default: null })
  params: any;

  @Prop({ required: true, index: true })
  hash: string;

  @Prop({ type: MongooseSchema.Types.Boolean })
  censored?: boolean;
}

export const EntitySchema = SchemaFactory.createForClass(Entity);
