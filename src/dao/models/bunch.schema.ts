import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export interface BunchDocument extends Document, Bunch {}

@Schema()
export class Bunch {
  @Prop({ enum: Status, index: true, default: Status.INITIAL })
  status?: Status;

  @Prop()
  error?: string;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: string | number;

  @Prop({ type: MongooseSchema.Types.Date, index: true })
  updatedAt?: string | number;

  @Prop({ type: MongooseSchema.Types.Date, index: true, default: Date.now })
  lastCheckAt?: string | number;
}

export const BunchSchema = SchemaFactory.createForClass(Bunch);
