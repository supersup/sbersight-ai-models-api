import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export interface QueryDocument extends Document, Query {}

@Schema()
export class Query {
  @Prop({ type: String, index: true })
  query?: string;

  @Prop({ type: String })
  userId?: string;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: string;
}

export const QuerySchema = SchemaFactory.createForClass(Query);
