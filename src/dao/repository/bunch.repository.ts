import { FilterQuery, Model, UpdateQuery } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Bunch, BunchDocument } from '../models/bunch.schema';

@Injectable()
class BunchRepository {
  constructor(
    @InjectModel(Bunch.name) private bunchModel: Model<BunchDocument>,
  ) {}

  create(queue: Bunch = {}) {
    const createdBunch = new this.bunchModel(queue);

    return createdBunch.save();
  }

  delete(id: string) {
    return this.bunchModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Bunch>) {
    return this.bunchModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  updateMany(
    filter: FilterQuery<BunchDocument>,
    update: UpdateQuery<BunchDocument>,
  ) {
    return this.bunchModel
      .updateMany(filter, update, { useFindAndModify: false })
      .exec();
  }

  findByIdAndUpdate(id: string, update?: UpdateQuery<BunchDocument>) {
    return this.bunchModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  async findAndUpdate(
    filter: FilterQuery<BunchDocument>,
    update: UpdateQuery<BunchDocument>,
    projection?: any,
  ) {
    const list = await this.find(filter, projection);
    this.updateMany(filter, update);
    return list;
  }

  findAll() {
    return this.bunchModel.find({}).exec();
  }

  find(filter: FilterQuery<BunchDocument>, projection?: any) {
    return this.bunchModel.find(filter, projection).exec();
  }

  findById(id: string, projection?: any) {
    try {
      return this.bunchModel.findById(id, projection).exec();
    } catch (e) {
      console.error(e);
    }
  }

  findOne(filter: Partial<Bunch>, projection?: any) {
    return this.bunchModel.findOne(filter, projection).exec();
  }

  deleteMany(filter: FilterQuery<BunchDocument>) {
    return this.bunchModel.deleteMany(filter).exec();
  }
}

export default BunchRepository;
