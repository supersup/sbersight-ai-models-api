import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery, FilterQuery } from 'mongoose';
import { Entity, EntityDocument } from '../models/entity.schema';

@Injectable()
class EntityRepository {
  constructor(
    @InjectModel(Entity.name) private entityModel: Model<EntityDocument>,
  ) {}

  create(queue: Entity) {
    const createdEntity = new this.entityModel(queue);

    return createdEntity.save();
  }

  delete(id: number) {
    return this.entityModel.findByIdAndDelete(id).exec();
  }

  update(
    filter: FilterQuery<EntityDocument>,
    update: UpdateQuery<typeof Entity>,
  ) {
    // .findByIdAndUpdate(id, update, { useFindAndModify: false })
    return this.entityModel
      .findOneAndUpdate(filter, update, { useFindAndModify: false })
      .exec();
  }

  updateMany(
    filter: FilterQuery<EntityDocument>,
    update: UpdateQuery<EntityDocument>,
  ) {
    return this.entityModel
      .updateMany(filter, update, { useFindAndModify: false })
      .exec();
  }

  findOneAndUpdate(
    filter?: FilterQuery<EntityDocument>,
    update?: UpdateQuery<EntityDocument>,
  ) {
    return this.entityModel
      .findOneAndUpdate(filter, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.entityModel.find({}).exec();
  }

  count(filter: FilterQuery<EntityDocument>, projection?: any) {
    return this.entityModel.count(filter, projection).exec();
  }

  find(filter: FilterQuery<EntityDocument>, projection?: any) {
    return this.entityModel.find(filter, projection).exec();
  }

  findById(id: string) {
    return this.entityModel.findById(id).exec();
  }

  findOne(filter: FilterQuery<EntityDocument>, projection?: any) {
    return this.entityModel.findOne(filter, projection).exec();
  }

  deleteMany(filter: FilterQuery<EntityDocument>) {
    return this.entityModel.deleteMany(filter).exec();
  }
}

export default EntityRepository;
