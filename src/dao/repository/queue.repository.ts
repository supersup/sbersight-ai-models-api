import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery, FilterQuery, QueryOptions } from 'mongoose';
import { Queue, QueueDocument } from '../models/queue.schema';

@Injectable()
class QueueRepository {
  constructor(
    @InjectModel(Queue.name) private queueModel: Model<QueueDocument>,
  ) {}

  create(queue: Queue) {
    const createdQueue = new this.queueModel(queue);

    return createdQueue.save();
  }

  delete(id: string) {
    return this.queueModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Queue>) {
    return this.queueModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  updateMany(
    filter: FilterQuery<QueueDocument>,
    update: UpdateQuery<QueueDocument>,
  ) {
    return this.queueModel
      .updateMany(filter, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.queueModel.find({}).exec();
  }

  find(filter: FilterQuery<QueueDocument>, projection?: any) {
    return this.queueModel.find(filter, projection).allowDiskUse(true).exec();
  }

  findById(id: string) {
    return this.queueModel.findById(id).exec();
  }

  findOne(filter?: FilterQuery<QueueDocument>, projection?: any) {
    return this.queueModel.findOne(filter, projection).exec();
  }

  findOneAndUpdate(
    filter?: FilterQuery<QueueDocument>,
    update?: UpdateQuery<typeof Queue>,
    options: QueryOptions = { useFindAndModify: false },
  ) {
    return this.queueModel.findOneAndUpdate(filter, update, options).exec();
  }

  findOneAndDelete(filter?: FilterQuery<QueueDocument>) {
    return this.queueModel.findOneAndDelete(filter).exec();
  }

  findByIdAndDelete(id: string) {
    return this.queueModel.findByIdAndDelete(id).exec();
  }

  deleteMany(filter: FilterQuery<QueueDocument>) {
    return this.queueModel.deleteMany(filter).exec();
  }

  count(filter?: FilterQuery<QueueDocument>) {
    return this.queueModel.count(filter);
  }
}

export default QueueRepository;
