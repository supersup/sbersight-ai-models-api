import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery, FilterQuery } from 'mongoose';
import { Query, QueryDocument } from '../models/query.schema';

@Injectable()
class QueueRepository {
  constructor(
    @InjectModel(Query.name) private queryModel: Model<QueryDocument>,
  ) {}

  create(query: Query) {
    const createdQueue = new this.queryModel(query);

    return createdQueue.save();
  }

  delete(id: string) {
    return this.queryModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Query>) {
    return this.queryModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.queryModel.find({}).exec();
  }

  find(filter: FilterQuery<QueryDocument>, projection?: any) {
    return this.queryModel.find(filter, projection).exec();
  }

  findById(id: string) {
    return this.queryModel.findById(id).exec();
  }

  findOne(filter?: FilterQuery<QueryDocument>, projection?: any) {
    return this.queryModel.findOne(filter, projection).exec();
  }

  findOneAndUpdate(
    filter?: FilterQuery<QueryDocument>,
    update?: UpdateQuery<typeof Query>,
  ) {
    return this.queryModel
      .findOneAndUpdate(filter, update, { useFindAndModify: false })
      .exec();
  }

  deleteMany(filter: FilterQuery<QueryDocument>) {
    return this.queryModel.deleteMany(filter).exec();
  }

  count(filter?: FilterQuery<QueryDocument>) {
    return this.queryModel.count(filter);
  }
}

export default QueueRepository;
