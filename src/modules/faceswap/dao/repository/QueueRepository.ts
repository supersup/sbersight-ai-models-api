import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery } from 'mongoose';
import { Queue, QueueDocument } from '../models/queue.schema';

@Injectable()
class QueueRepository {
  constructor(
    @InjectModel(Queue.name) private queueModel: Model<QueueDocument>,
  ) {}

  create(queue: Queue) {
    const createdQueue = new this.queueModel(queue);

    return createdQueue.save();
  }

  delete(id: number) {
    return this.queueModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Queue>) {
    return this.queueModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.queueModel.find({}).exec();
  }

  find(filter: Partial<Queue>, projection?: any) {
    return this.queueModel.find(filter, projection).exec();
  }

  findById(id: string) {
    return this.queueModel.findById(id).exec();
  }

  findOne(filter?: Partial<Queue>, projection?: any) {
    return this.queueModel.findOne(filter, projection).exec();
  }

  deleteMany(filter: Partial<Queue>) {
    return this.queueModel.deleteMany(filter).exec();
  }

  count() {
    return this.queueModel.count();
  }
}

export default QueueRepository;
