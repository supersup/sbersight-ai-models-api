import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export interface EntityDocument extends Document, Entity {}

type S3Credentials = {
  endpoint_url;
  bucket;
  namespace;
  access_key_id;
  security_access_key;
};

@Schema()
export class Entity {
  @Prop({ required: true })
  bunchId: string;

  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: Date;

  @Prop()
  result?: string;

  @Prop()
  error?: string;

  @Prop()
  source_image: string;

  @Prop()
  target_image: string;

  @Prop()
  path_to_video: string;

  @Prop()
  folder_to_save: string;

  @Prop({ type: MongooseSchema.Types.Map })
  s3_credentials: S3Credentials;
}

export const EntityModel = SchemaFactory.createForClass(Entity);
