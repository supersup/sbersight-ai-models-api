import { LeanDocument } from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import got, { getHooks } from 'src/util/got';
import { Config } from 'src/config/Configuration';
import { FSPushToQueueReqDto } from './dto/entity.dto';
import { ModelResDto } from './dto/aiModel.dto';
import BunchRepository from './dao/repository/BunchRepository';
import EntityRepository from './dao/repository/EntityRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { EntityDocument } from './dao/models/entity.schema';
import { s3Credentials } from '../../util/s3';
import { Status } from 'src/dto/status.dto';

@Injectable()
export class FaceswapService {
  constructor(
    private configService: ConfigService<Config>,
    private bunchRepository: BunchRepository,
    private entityRepository: EntityRepository,
    private queueRepository: QueueRepository,
  ) {}

  async pushToQueue({ items }: FSPushToQueueReqDto) {
    try {
      const bunch = await this.bunchRepository.create();

      items.forEach((item) => {
        this.entityRepository.create({
          bunchId: bunch._id,
          source_image: item.source_image,
          path_to_video: item.path_to_video,
          target_image: item.target_image,
          folder_to_save: item.folder_to_save,
          s3_credentials: item.s3_credentials,
        });
      });

      this.queueRepository.create({
        bunchId: bunch._id,
      });

      return {
        bunchId: bunch._id,
      };
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getBunchStatus(bunchId: string) {
    return this.bunchRepository.findById(bunchId, { status: true });
  }

  async getBunchEntities(bunchId: string) {
    return this.entityRepository.find({ bunchId });
  }

  async getBunchEntity(entityId: string) {
    return this.entityRepository.findById(entityId);
  }

  async processEntity(entity: LeanDocument<EntityDocument>) {
    try {
      const aiModelUrl = this.configService.get('faceswap.aiModelHost', {
        infer: true,
      });
      const deployId = this.configService.get('faceswap.deployId', {
        infer: true,
      });

      const result = await got
        .extend(getHooks('faceswap'))
        .post(aiModelUrl.replace(/\{\w+\}/g, deployId), {
          json: {
            ...entity,
            s3_credentials: s3Credentials,
          },
        })
        .json<ModelResDto>();

      if (result.ok) {
        this.entityRepository.update(entity._id, {
          result: result.path_to_result || result.result,
          status: Status.SUCCESS,
        });
      } else {
        this.entityRepository.update(entity._id, {
          error: result.error,
          status: Status.FAIL,
        });
      }
    } catch (e) {
      this.entityRepository.update(entity._id, {
        error: e.message,
        status: Status.FAIL,
      });
    }
  }

  @Cron(CronExpression.EVERY_SECOND)
  async processQueue() {
    const isQueueFilled = Boolean(await this.queueRepository.count());
    const queues = await this.queueRepository.findAll();
    const initialQueues = [];
    const hasInitialQueues = queues.some((i) => {
      if (i.status === 'INITIAL') {
        initialQueues.push(i);

        return true;
      }
    });

    if (isQueueFilled && hasInitialQueues) {
      const { bunchId, _id: queueId } = initialQueues[0];
      this.queueRepository.update(queueId, {
        status: 'PROCESSING',
      });
      const entities = await this.entityRepository.find({
        bunchId: bunchId,
      });

      this.bunchRepository.update(bunchId, { status: Status.PROCESSING });

      await Promise.all(
        entities.map((entity) => this.processEntity(entity.toObject())),
      );

      this.bunchRepository.update(bunchId, { status: Status.SUCCESS });
      this.queueRepository.delete(queueId);
    }
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async pruneDatabase() {
    const DAY = 86_400_000_000;
    const orCondition = [
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        updatedAt: { $lte: new Date(Date.now() - DAY) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createdAt: { $lte: new Date(Date.now() - DAY) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
    ];
    const expiredQueueItems = await this.queueRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
    const expiredBunches = await this.bunchRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });

    expiredQueueItems.forEach((item) => item.delete());
    expiredBunches.forEach((bunch) => {
      this.entityRepository
        .find({ bunchId: bunch._id })
        .then((items) => items.forEach((i) => i.delete()));
      bunch.delete();
    });
  }
}
