import { Test, TestingModule } from '@nestjs/testing';
import { FaceswapController } from './faceswap.controller';
import { FaceswapService } from './faceswap.service';

describe('FaceswapController', () => {
  let faceswapController: FaceswapController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [FaceswapController],
      providers: [FaceswapService],
    }).compile();

    faceswapController = app.get<FaceswapController>(FaceswapController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const target = faceswapController.pushToQueue({
        items: [
          {
            source_image: 'asdg',
            target_image: 'asdg',
            path_to_video: 'asdg',
            folder_to_save: 'asdg',
            s3_credentials: {
              endpoint_url: 'asdg',
              bucket: 'asdg',
              namespace: 'asdg',
              access_key_id: 'asdg',
              security_access_key: 'asdg',
            },
          },
        ],
      });

      expect(target).toContain({ bunchId: '6197b57e49ad29f8d0e2c184' });
    });
  });
});
