export type S3Credemtials = {
  s3_credentials: {
    endpoint_url;
    bucket;
    namespace;
    access_key_id;
    security_access_key;
  };
};

export type ModelParamsDto = {
  source_image: string;
  target_image: string;
  path_to_video: string;
  folder_to_save: string;
  s3_credentials: S3Credemtials;
};

export type AuthParamsDto = {
  authToken: string;
  'x-workspace-id': string;
};

export interface ModelResDtoImpl {
  path_to_result: string;
  error?: string;
  result?: string;
}

export class ModelResDto implements ModelResDtoImpl {
  path_to_result: string;
  error?: string;
  result?: string;
  ok: string;
}
