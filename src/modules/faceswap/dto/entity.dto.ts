import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Status } from 'src/dto/status.dto';

export interface S3CredentialsImpl {
  endpoint_url: string;
  bucket: string;
  namespace: string;
  access_key_id: string;
  security_access_key: string;
}

export class S3Credentials implements S3CredentialsImpl {
  @ApiProperty({ required: true })
  endpoint_url: string;

  @ApiProperty({ required: true })
  bucket: string;

  @ApiProperty({ required: true })
  namespace: string;

  @ApiProperty({ required: true })
  access_key_id: string;

  @ApiProperty({ required: true })
  security_access_key: string;
}

export interface EntityDtoImpl {
  _id: string;
  status: Status;
  source_image: string;
  target_image: string;
  path_to_video: string;
  folder_to_save: string;
  s3_credentials: S3CredentialsImpl;
}

export class EntityDto implements EntityDtoImpl {
  @ApiProperty({
    description: 'Уникальный идентификатор сущности',
    required: true,
  })
  _id: string;

  @ApiProperty({
    description: 'Статус',
    enum: Status,
    required: true,
  })
  status: Status;

  @ApiProperty({
    description: 'Исходная картинка',
    required: true,
  })
  source_image: string;

  @ApiProperty({
    description: 'Целевая картинка',
    required: true,
  })
  target_image: string;

  @ApiProperty({
    description: 'Путь до видео',
    required: true,
  })
  path_to_video: string;

  @ApiProperty({
    description: 'Путь для сохранения',
    required: true,
  })
  folder_to_save: string;

  @ApiProperty({
    description: 'Креды для s3',
    type: S3Credentials,
    required: true,
  })
  s3_credentials: S3Credentials;
}

export class FSPushToQueueReqItemsDto extends OmitType(EntityDto, [
  '_id',
  'status',
] as const) {}

export class FSPushToQueueReqDto {
  @ApiProperty({
    description: 'Массив изображений',
    type: [FSPushToQueueReqItemsDto],
    required: true,
  })
  items: FSPushToQueueReqItemsDto[];
}

export interface PushToQueueResImpl {
  bunchId: string;
}

export class PushToQueueResDto implements PushToQueueResImpl {
  @ApiProperty({ description: 'Уникальный идентификатор пакета' })
  bunchId: string;
}
