import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import Configuration from 'src/config/Configuration';
import { Entity, EntityModel } from './dao/models/entity.schema';
import { Bunch, BunchModel } from './dao/models/bunch.schema';
import { Queue, QueueModel } from './dao/models/queue.schema';
import EntityRepository from './dao/repository/EntityRepository';
import BunchRepository from './dao/repository/BunchRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { FaceswapController } from './faceswap.controller';
import { FaceswapService } from './faceswap.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [Configuration.configFactory],
    }),
    MongooseModule.forFeature(
      [
        { name: Entity.name, schema: EntityModel },
        { name: Bunch.name, schema: BunchModel },
        { name: Queue.name, schema: QueueModel },
      ],
      process.env.FS_DB_NAME,
    ),
  ],
  controllers: [FaceswapController],
  providers: [
    FaceswapService,
    EntityRepository,
    BunchRepository,
    QueueRepository,
  ],
})
export class FaceswapModule {}
