import { Body, Controller, HttpStatus, Post, Get, Param } from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FaceswapService } from './faceswap.service';
import {
  EntityDto,
  FSPushToQueueReqDto,
  PushToQueueResDto,
} from './dto/entity.dto';
import { BunchStatusReqDto, BunchStatusResDto } from './dto/bunch.dto';

@ApiTags('faceswap')
@Controller('faceswap')
export class FaceswapController {
  constructor(private styletransferService: FaceswapService) {}

  @ApiOperation({ summary: 'Поместить задачу в очередь на обработку' })
  @ApiResponse({ status: 200, type: PushToQueueResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Post('/push')
  async pushToQueue(@Body() body: FSPushToQueueReqDto) {
    return this.styletransferService.pushToQueue(body);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: BunchStatusResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/status')
  async bunchStatus(@Param() params: BunchStatusReqDto) {
    return this.styletransferService.getBunchStatus(params.bunchId);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: [EntityDto] })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities')
  async bunchEntities(@Param() params: BunchStatusReqDto) {
    return this.styletransferService.getBunchEntities(params.bunchId);
  }

  @ApiOperation({ summary: 'Получение сущности' })
  @ApiResponse({ status: 200, type: EntityDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities/:entityId')
  async getBunchEntity(@Param() params: BunchStatusReqDto) {
    return this.styletransferService.getBunchEntity(params.entityId);
  }
}
