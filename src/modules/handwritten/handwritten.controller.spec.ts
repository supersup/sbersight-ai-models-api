import { Test, TestingModule } from '@nestjs/testing';
import { HandwrittenController } from './handwritten.controller';
import { HandwrittenService } from './handwritten.service';

describe('HandwrittenController', () => {
  let handwrittenController: HandwrittenController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [HandwrittenController],
      providers: [HandwrittenService],
    }).compile();

    handwrittenController = app.get<HandwrittenController>(
      HandwrittenController,
    );
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const target = handwrittenController.pushToQueue({
        items: [
          {
            image: 'asdgasdgas',
            filename: 'asdgasdg',
          },
        ],
      });

      expect(target).toContain({ bunchId: '6197b57e49ad29f8d0e2c184' });
    });
  });
});
