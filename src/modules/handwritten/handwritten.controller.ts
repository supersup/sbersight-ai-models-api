import {
  BadRequestException,
  Controller,
  HttpStatus,
  Param,
  Body,
  Post,
  Get,
} from '@nestjs/common';
import {
  ApiUnauthorizedResponse,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { HandwrittenService } from './handwritten.service';
import {
  EntityDto,
  HWPushToQueueReqDto,
  PushToQueueResDto,
} from './dto/entity.dto';
import { BunchStatusReqDto, BunchStatusResDto } from './dto/bunch.dto';

@ApiTags('handwritten')
@Controller('handwritten')
export class HandwrittenController {
  constructor(private handwrittenService: HandwrittenService) {}

  @ApiOperation({ summary: 'Поместить задачу в очередь на обработку' })
  @ApiResponse({ status: 200, type: PushToQueueResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Post('/push')
  async pushToQueue(@Body() body: HWPushToQueueReqDto) {
    return this.handwrittenService.pushToQueue(body);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: BunchStatusResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/status')
  async bunchStatus(@Param() params: BunchStatusReqDto) {
    return this.handwrittenService.getBunchStatus(params.bunchId);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: [EntityDto] })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities')
  async bunchEntities(@Param() params: BunchStatusReqDto) {
    return this.handwrittenService.getBunchEntities(params.bunchId);
  }

  @ApiOperation({ summary: 'Получение сущности' })
  @ApiResponse({ status: 200, type: EntityDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities/:entityId')
  async getBunchEntity(@Param() params: BunchStatusReqDto) {
    const entity = await this.handwrittenService.getBunchEntity(
      params.entityId,
    );

    if (!entity) {
      throw new BadRequestException(`No such pocket id: ${params.entityId}`);
    }

    return entity;
  }
}
