type Prediction = {
  id: number;
  mask: string;
  text: string;
};

export interface ModelResDtoImpl {
  predictions?: string | Prediction[];
}

export class ModelResDto implements ModelResDtoImpl {
  predictions;
}
