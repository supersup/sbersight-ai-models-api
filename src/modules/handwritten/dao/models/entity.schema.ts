import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export class Prediction {
  @Prop({ type: Number })
  id: number;

  @Prop({ type: String })
  mask: string;

  @Prop({ type: String })
  text: string;
}

export interface EntityDocument extends Document, Entity {}

@Schema()
export class Entity {
  @Prop({ type: String, required: true })
  bunchId: string;

  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: string;

  @Prop()
  error?: string;

  @Prop({ type: [MongooseSchema.Types.Mixed] })
  result?: Prediction[];

  @Prop({ required: true })
  image: string;

  @Prop()
  filename?: string;
}

export const EntitySchema = SchemaFactory.createForClass(Entity);
