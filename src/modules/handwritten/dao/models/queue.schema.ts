import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export interface QueueDocument extends Document, Queue {}

enum Status {
  INITIAL = 'INITIAL',
  PROCESSING = 'PROCESSING',
}

@Schema()
export class Queue {
  @Prop({ type: String, required: true })
  bunchId: string;

  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: string;
}

export const QueueSchema = SchemaFactory.createForClass(Queue);
