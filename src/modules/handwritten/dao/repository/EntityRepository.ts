import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery } from 'mongoose';
import { Entity, EntityDocument } from '../models/entity.schema';

@Injectable()
class EntityRepository {
  constructor(
    @InjectModel(Entity.name) private entityModel: Model<EntityDocument>,
  ) {}

  create(queue: Entity) {
    const createdEntity = new this.entityModel(queue);

    return createdEntity.save();
  }

  delete(id: number) {
    return this.entityModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Entity>) {
    return this.entityModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.entityModel.find({}).exec();
  }

  find(filter: Partial<Entity>, projection?: any) {
    return this.entityModel.find(filter, projection).exec();
  }

  findById(id: string) {
    return this.entityModel.findById(id).exec();
  }

  findOne(filter: Partial<Entity>, projection?: any) {
    return this.entityModel.findOne(filter, projection).exec();
  }

  deleteMany(filter: Partial<Entity>) {
    return this.entityModel.deleteMany(filter).exec();
  }
}

export default EntityRepository;
