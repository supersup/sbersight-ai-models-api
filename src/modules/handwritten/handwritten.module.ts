import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import Configuration from 'src/config/Configuration';
import { Entity, EntitySchema } from './dao/models/entity.schema';
import { Bunch, BunchSchema } from './dao/models/bunch.schema';
import { Queue, QueueSchema } from './dao/models/queue.schema';
import EntityRepository from './dao/repository/EntityRepository';
import BunchRepository from './dao/repository/BunchRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { HandwrittenController } from './handwritten.controller';
import { HandwrittenService } from './handwritten.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [Configuration.configFactory],
    }),
    MongooseModule.forFeature(
      [
        { name: Entity.name, schema: EntitySchema },
        { name: Bunch.name, schema: BunchSchema },
        { name: Queue.name, schema: QueueSchema },
      ],
      process.env.HW_DB_NAME,
    ),
  ],
  controllers: [HandwrittenController],
  providers: [
    HandwrittenService,
    EntityRepository,
    BunchRepository,
    QueueRepository,
  ],
})
export class HandwrittenModule {}
