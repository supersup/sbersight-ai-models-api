import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import {
  makeCounterProvider,
  makeGaugeProvider,
} from '@willsoto/nestjs-prometheus';
import { ElasticsearchModule } from '@nestjs/elasticsearch';
import Configuration from 'src/config/Configuration';
import { Entity, EntitySchema } from 'src/dao/models/entity.schema';
import { Bunch, BunchSchema } from 'src/dao/models/bunch.schema';
import { Queue, QueueSchema } from 'src/dao/models/queue.schema';
import { Query, QuerySchema } from 'src/dao/models/query.schema';
import EntityRepository from 'src/dao/repository/entity.repository';
import BunchRepository from 'src/dao/repository/bunch.repository';
import QueueRepository from 'src/dao/repository/queue.repository';
import QueryRepository from 'src/dao/repository/query.repository';
import { Text2ImageController } from './text2image.controller';
import { Text2ImageService } from './text2image.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [Configuration.configFactory],
    }),
    MongooseModule.forFeature(
      [
        { name: Entity.name, schema: EntitySchema },
        { name: Bunch.name, schema: BunchSchema },
        { name: Queue.name, schema: QueueSchema },
        { name: Query.name, schema: QuerySchema },
      ],
      process.env.T2I_DB_NAME,
    ),
    ElasticsearchModule.register({
      node: process.env.ELASTIC_HOST,
      auth: {
        username: process.env.ELASTIC_USER,
        password: process.env.ELASTIC_PASSWORD,
      },
    }),
  ],
  controllers: [Text2ImageController],
  providers: [
    Text2ImageService,
    EntityRepository,
    BunchRepository,
    QueueRepository,
    QueryRepository,
    makeCounterProvider({
      name: 'ai_models_generation_requests',
      help: 'ai_models_api request for generation',
    }),
    makeCounterProvider({
      name: 'ai_models_generation_responses',
      help: 'ai_models_api response for generation',
    }),
    makeGaugeProvider({
      name: 'ai_models_generation_queue_count',
      help: 'ai_models_api queue for generation count',
    }),
    makeGaugeProvider({
      name: 'ai_models_generation_time',
      help: 'ai_models_api time for generation',
    }),
  ],
})
export class Text2ImageModule {}
