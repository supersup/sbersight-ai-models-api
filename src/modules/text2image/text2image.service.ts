import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { InjectMetric } from '@willsoto/nestjs-prometheus';
import { Counter, Gauge } from 'prom-client';
import { Config } from 'src/config/Configuration';
import got, { getHooks } from 'src/util/got';
import { PushToQueueReqDto } from 'src/dto/entity.dto';
import {
  GetQueueItemReqDto,
  ModelResponseDto,
  SetResultReqDto,
  TaskStatus,
} from './dto/model.dto';
import BunchRepository from 'src/dao/repository/bunch.repository';
import EntityRepository from 'src/dao/repository/entity.repository';
import QueueRepository from 'src/dao/repository/queue.repository';
import QueryRepository from 'src/dao/repository/query.repository';
import { EntityDocument } from 'src/dao/models/entity.schema';
import { CheckQueueReqDto } from './dto/text2image.dto';
import { Status } from 'src/dto/status.dto';
// import { s3Stream, s3Bucket } from 'src/util/s3';
import { NoContentException } from 'src/exceptions';

@Injectable()
export class Text2ImageService {
  constructor(
    @InjectMetric('ai_models_generation_requests')
    public request: Counter<string>,
    @InjectMetric('ai_models_generation_responses')
    public response: Counter<string>,
    @InjectMetric('ai_models_generation_queue_count')
    public queue: Gauge<string>,
    @InjectMetric('ai_models_generation_time')
    public time: Gauge<string>,
    private readonly elasticsearchService: ElasticsearchService,
    private configService: ConfigService<Config>,
    private bunchRepository: BunchRepository,
    private entityRepository: EntityRepository,
    private queueRepository: QueueRepository,
    private queryRepository: QueryRepository,
  ) {}

  async checkHash(hash: string) {
    const entitiesCount = await this.entityRepository.count({ hash });

    if (!entitiesCount) {
      return {
        exist: false,
        available: true,
        count: entitiesCount,
        bunchId: null,
      };
    } else if (entitiesCount < 100) {
      return {
        exist: true,
        available: true,
        count: entitiesCount,
        bunchId: null,
      };
    } else {
      const firstSuccessEntity = await this.entityRepository.findOne({ hash });

      return {
        exist: true,
        available: false,
        count: entitiesCount,
        bunchId: firstSuccessEntity.bunchId,
      };
    }
  }

  async checkQueue(params: CheckQueueReqDto) {
    try {
      const queueCount = await this.queueRepository.count({
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: Status.INITIAL,
      });

      const startOfTheDay = new Date();
      startOfTheDay.setHours(0, 0, 0, 0);

      const endOfTheDay = new Date();
      endOfTheDay.setHours(0, 0, 0, 0);
      endOfTheDay.setHours(23, 59, 59, 999);

      // const dayCount = await this.queryRepository.count({
      //   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //   // @ts-ignore
      //   createdAt: { $gte: startOfTheDay, $lte: endOfTheDay },
      // });

      this.queue.set(queueCount);
      return { count: queueCount, dayCount: 0 };
    } catch {
      return {};
    }
  }

  async clearQueue() {
    const queueItems = await this.queueRepository.find({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      status: {
        $in: [Status.INITIAL],
      },
    });

    queueItems.forEach((queue) => {
      this.bunchRepository.findById(queue.bunchId).then((bunch) => {
        this.entityRepository
          .find({ bunchId: bunch._id })
          .then((items) => items.forEach((i) => i.delete()));
        bunch.delete();
      });

      queue.delete();
    });

    return true;
  }

  async pushToQueue(params: PushToQueueReqDto) {
    try {
      const bunch = await this.bunchRepository.create();
      params.items.forEach((item) => {
        this.elasticsearchService
          .index({
            index: 'query',
            body: {
              '@timestamp': Date.now(),
              query: item.query,
              userId: String(item.user_id),
            },
          })
          .catch((e) => console.error(e));
        this.queryRepository.create({
          query: item.query,
          userId: String(item.user_id),
        });

        this.entityRepository.create({
          bunchId: bunch._id,
          params: item,
          // TODO: HASH is temporary solution, its will be removed later
          hash: item.hash,
        });
      });

      this.queueRepository.create({
        bunchId: bunch._id,
      });

      this.request.inc();
      return {
        bunchId: bunch._id,
      };
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getBunchStatus(bunchId: string) {
    return this.bunchRepository.findByIdAndUpdate(bunchId, {
      lastCheckAt: Date.now(),
    });
  }

  async getBunchEntities(bunchId: string) {
    return this.entityRepository.find({ bunchId }, [
      'bunchId',
      'censored',
      'createdAt',
      'error',
      'hash',
      'response',
      'status',
      'updatedAt',
      '_id',
    ]);
  }

  async getBunchEntity(entityId: string) {
    return this.entityRepository.findById(entityId);
  }

  async processEntity(entity: EntityDocument) {
    try {
      this.entityRepository.update(entity._id, {
        updatedAt: Date.now(),
        status: Status.PROCESSING,
      });
      const aiModelUrl = this.configService.get('text2image.aiModelHost', {
        infer: true,
      });
      const deployId = this.configService.get('text2image.deployId', {
        infer: true,
      });

      const result = await got
        .extend(getHooks('text2image'))
        .post(aiModelUrl.replace(/\{\w+\}/g, deployId), {
          json: {
            instances: entity.params,
          },
        })
        .json<ModelResponseDto>();

      if (result.generated_images) {
        this.entityRepository.update(entity._id, {
          response: result.generated_images,
          status: Status.SUCCESS,
          updatedAt: Date.now(),
        });
      } else {
        this.entityRepository.update(entity._id, {
          updatedAt: Date.now(),
          error: result,
          status: Status.FAIL,
        });
      }
    } catch (e) {
      this.entityRepository.update(entity._id, {
        error: [e.message],
        updatedAt: Date.now(),
        status: Status.FAIL,
      });
    }
  }

  // TODO: Now another workflow works
  // @Cron(CronExpression.EVERY_SECOND)
  async processQueue() {
    const queueCount = await this.queueRepository.count();
    const isQueueFilled = Boolean(queueCount);
    const queues = await this.queueRepository.findAll();
    const initialQueues = [];
    const hasInitialQueues = queues.some((i) => {
      if (i.status === 'INITIAL') {
        initialQueues.push(i);

        return true;
      }
    });

    if (isQueueFilled && hasInitialQueues) {
      const { bunchId, _id: queueId } = initialQueues[0];
      this.queueRepository.update(queueId, {
        updatedAt: Date.now(),
        status: 'PROCESSING',
      });
      const entities = await this.entityRepository.find({
        bunchId: bunchId,
      });

      this.bunchRepository.update(bunchId, {
        updatedAt: Date.now(),
        status: Status.PROCESSING,
      });

      await Promise.all(
        entities.map(async (entity) => this.processEntity(entity)),
      );

      this.bunchRepository.update(bunchId, {
        status: Status.SUCCESS,
        updatedAt: Date.now(),
      });
      this.queueRepository.delete(queueId);
    }
  }

  async getQueueItem(params: GetQueueItemReqDto) {
    try {
      const freshItem = await this.queueRepository.findOneAndUpdate(
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        { status: Status.INITIAL },
        {
          status: Status.PROCESSING,
          updatedAt: Date.now(),
        },
        {
          sort: { createdAt: 'asc' },
          useFindAndModify: false,
        },
      );

      if (freshItem) {
        const bunchId = freshItem?.bunchId;
        const queueId = freshItem?._id;
        const entities = await this.entityRepository.find({
          bunchId: bunchId,
        });

        if (!entities.length) {
          throw new NoContentException();
        }

        if (entities[0]) {
          this.bunchRepository.update(bunchId, {
            status: Status.PROCESSING,
            updatedAt: Date.now(),
          });
          this.entityRepository.update(
            { bunchId: entities[0].bunchId },
            {
              status: Status.PROCESSING,
              updatedAt: Date.now(),
            },
          );

          const defaultTask = entities[0].params.init_image
            ? TaskStatus.INPAINTING
            : TaskStatus.GENERATE;

          return {
            query_id: queueId,
            task: entities[0].params.task
              ? entities[0].params.task
              : defaultTask,
            ...entities[0].params,
          };
        }
      }

      return {
        task: TaskStatus.NONE,
      };
    } catch (e) {
      console.error(e);
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async setProcessedEntity(params: SetResultReqDto) {
    try {
      this.response.inc();
      const queueItem = await this.queueRepository.findByIdAndDelete(
        params.query_id,
      );

      if (!queueItem) {
        return null;
      }

      if (params.generated_images.length) {
        const bunch = await this.bunchRepository.findByIdAndUpdate(
          queueItem.bunchId,
          {
            status: Status.SUCCESS,
            updatedAt: Date.now(),
          },
        );

        const dif = (Date.now() - Number(new Date(bunch.updatedAt))) / 1000;

        this.time.set(dif);
        this.entityRepository.findOneAndUpdate(
          {
            bunchId: queueItem.bunchId,
          },
          {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            response: params.generated_images,
            censored: params.censored,
            status: Status.SUCCESS,
            updatedAt: Date.now(),
          },
        );

        // try {
        //   const fileBuffer = Buffer.from(params.generated_images[0], 'base64');
        //   const filename =
        //     entity.params.query.length > 500
        //       ? entity.params.query.slice(0, 500)
        //       : entity.params.query;
        //   s3Stream
        //     .putObject({
        //       Bucket: s3Bucket,
        //       Key: `${filename}.${entity.params.hash}.png`,
        //       Body: fileBuffer,
        //     })
        //     .promise();
        // } catch (error) {
        //   console.error(error);
        // }
      } else {
        this.entityRepository.findOneAndUpdate(
          {
            bunchId: queueItem.bunchId,
          },
          {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            error: params,
            status: Status.FAIL,
          },
        );
      }

      return true;
    } catch (e) {
      console.error(e);
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async pruneProcessingItems() {
    const THREE_MINUTES = 80_000;
    const FORTY_FIVE_MINUTES = 2_400_000;
    const orCondition = [
      {
        updatedAt: { $lte: new Date(Date.now() - FORTY_FIVE_MINUTES) },
        status: {
          $in: [Status.PROCESSING],
        },
      },
    ];

    await this.queueRepository.deleteMany({
      // FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });

    this.bunchRepository.updateMany(
      // FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      { $or: orCondition },
      {
        status: Status.SUCCESS,
        updatedAt: Date.now(),
      },
    );

    const irrelevantInitialBunches = await this.bunchRepository.findAndUpdate(
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        lastCheckAt: { $lte: new Date(Date.now() - THREE_MINUTES) },
        status: {
          $in: [Status.INITIAL],
        },
      },
      {
        status: Status.FAIL,
        error: 'User did not wait for a response',
      },
      ['_id'],
    );
    this.bunchRepository.findAndUpdate(
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        lastCheckAt: { $lte: new Date(Date.now() - THREE_MINUTES) },
        status: {
          $in: [Status.PROCESSING],
        },
      },
      {
        status: Status.FAIL,
        error: 'User did not wait for a response',
      },
      ['_id'],
    );
    // FIXME: fix types of filter
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.queueRepository.deleteMany({
      bunchId: {
        $in: irrelevantInitialBunches.map((i) => i._id),
      },
    });

    this.entityRepository.updateMany(
      // FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      { $or: orCondition },
      {
        error: ['Is too long in proccess'],
        status: Status.FAIL,
        updatedAt: Date.now(),
      },
    );
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async pruneDatabase() {
    const HOUR = 7_200_000;
    const orCondition = [
      {
        updatedAt: { $lte: new Date(Date.now() - HOUR) },
        status: {
          $in: [Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
      {
        createdAt: { $lte: new Date(Date.now() - HOUR) },
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
    ];

    // const expiredQueueItems = await this.queueRepository.find({
    await this.queueRepository.deleteMany({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
    // const expiredBunches = await this.bunchRepository.find({
    await this.bunchRepository.deleteMany({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
    await this.entityRepository.deleteMany({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
  }
}
