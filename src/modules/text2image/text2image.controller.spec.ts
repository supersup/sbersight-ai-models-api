import { Test, TestingModule } from '@nestjs/testing';
import { Text2ImageController } from './text2image.controller';
import { Text2ImageService } from './text2image.service';

describe('HandwrittenController', () => {
  let text2imageController: Text2ImageController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [Text2ImageController],
      providers: [Text2ImageService],
    }).compile();

    text2imageController = app.get<Text2ImageController>(Text2ImageController);

    console.log(text2imageController);
  });

  // describe('root', () => {
  //   it('should return "Hello World!"', () => {
  //     const target = handwrittenController.pushToQueue({
  //       items: [
  //         {
  //           image: 'asdgasdgas',
  //           filename: 'asdgasdg',
  //         },
  //       ],
  //     });

  //     expect(target).toContain({ bunchId: '6197b57e49ad29f8d0e2c184' });
  //   });
  // });
});
