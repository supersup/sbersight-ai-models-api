import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsInt, IsString } from 'class-validator';
import { BunchStatusReqDto } from 'src/dto/bunch.dto';
import { EntityReqDto } from 'src/dto/entity.dto';
import { QueueType } from './model.dto';

export class CheckHashReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;

  @ApiProperty({
    description: 'хэщ сущности',
    type: String,
  })
  @IsString()
  hash: string;
}

export class CheckHashResDto {
  @ApiProperty({
    description: 'Индикатор наличия сущностей с заданным хешом',
    type: Boolean,
  })
  @IsBoolean()
  exist: boolean;

  @ApiProperty({
    description: 'Индикатор возможности обработки',
    type: Boolean,
  })
  @IsBoolean()
  available: boolean;

  @ApiProperty({
    description: 'Счетчик сущностей с заданным хешом',
    type: Number,
  })
  @IsInt()
  count: number;

  @ApiProperty({
    description: 'Обработанная сущность с заданным хешом',
    type: String,
    nullable: true,
  })
  @IsString()
  bunchId: string;
}

export class CheckQueueReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}

export class CheckQueueResDto {
  @ApiProperty({
    description: 'Сущностей в очереди',
    type: Number,
  })
  @IsInt()
  count: number;
}

export class T2IBunchStatusReqDto extends BunchStatusReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}

export class T2IEntityReqDto extends EntityReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}
