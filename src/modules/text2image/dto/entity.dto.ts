import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PushToQueueReqDto } from 'src/dto/entity.dto';
import { QueueType } from './model.dto';

export class T2IPushToQueueReqDto extends PushToQueueReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}
