import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsString,
  ValidateNested,
} from 'class-validator';

export class InstancesModelRequestDto {
  @IsString()
  query: string;

  @IsInt()
  num_images: number;

  @IsString()
  aspect_ratio: string;

  @IsInt()
  num_steps: number;

  @IsString()
  guidance_scale: number;
}

export class ModelRequestDto {
  @ValidateNested()
  instances: InstancesModelRequestDto;
}

export class ModelResponseDto {
  @IsBoolean()
  ok: boolean;

  @IsString({ each: true })
  generated_images: string[];
}

export enum QueueType {
  GENERATE = 'generate',
  INPAINTING = 'inpainting',
  MIXING = 'mixing',
}

export class GetQueueItemReqDto {
  @ApiProperty({ description: 'Shared secret', type: String })
  @IsString()
  token: string;

  @ApiProperty({ description: 'ID воркера', type: String })
  @IsString()
  worker_id: string;
}

export enum TaskStatus {
  NONE = 'none',
  INPAINTING = 'inpainting',
  GENERATE = 'generate',
}

export class GetQueueItemResDto {
  @ApiProperty({
    description: 'Флаг наличия заданий в очереди (очередь пуста)',
    enum: TaskStatus,
  })
  @IsEnum(TaskStatus)
  task: TaskStatus;

  @ApiProperty({ description: 'ID запроса', type: String })
  @IsString()
  query_id: string;

  @ApiProperty({ description: 'Текст запроса на генерацию', type: String })
  @IsString()
  query: string;

  @ApiProperty({ description: 'Ширина изображения', type: Number })
  @IsInt()
  width: number;

  @ApiProperty({ description: 'Высота изображения', type: Number })
  @IsInt()
  height: number;

  @ApiProperty({
    description: 'Параметр модели (кол-во степов диффузии)',
    type: Number,
  })
  @IsInt()
  num_steps: number;

  @ApiProperty({
    description: 'Сколько изображений генерировать',
    type: Number,
  })
  @IsInt()
  num_images: number;

  @ApiProperty({ description: 'Параметр модели', type: Number })
  @IsInt()
  guidance_scale: number;
}

export class SetResultReqDto {
  @ApiProperty({ description: 'Shared secret', type: String })
  @IsString()
  token: string;

  @ApiProperty({ description: 'ID воркера', type: String })
  @IsString()
  worker_id: string;

  @ApiProperty({ description: 'ID запроса', type: String })
  @IsString()
  query_id: string;

  @ApiProperty({ description: 'Результат обработки модели', type: String })
  @IsString({ each: true })
  generated_images: Array<string>;

  @ApiProperty({ description: 'Цензура', type: Boolean })
  censored: boolean;
}
