import { Controller, HttpStatus, Param, Body, Post, Get } from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { NoContentException } from 'src/exceptions/NoContentException';
import { Text2ImageService } from './text2image.service';
import { EntityDto, PushToQueueResDto } from 'src/dto/entity.dto';
import { BunchStatusResDto } from 'src/dto/bunch.dto';
import {
  GetQueueItemReqDto,
  GetQueueItemResDto,
  SetResultReqDto,
} from './dto/model.dto';
import {
  CheckHashReqDto,
  CheckHashResDto,
  T2IBunchStatusReqDto,
  T2IEntityReqDto,
  CheckQueueReqDto,
  CheckQueueResDto,
} from './dto/text2image.dto';
import { T2IPushToQueueReqDto } from './dto/entity.dto';

@ApiTags('text2image')
@Controller('text2image')
export class Text2ImageController {
  constructor(private text2imageService: Text2ImageService) {}

  @ApiOperation({ summary: 'Проверить наличие сущностей по заданному хешу' })
  @ApiResponse({ status: 200, type: CheckHashResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Get('/:queueType/check_hash/:hash')
  async checkHash(@Param() { hash }: CheckHashReqDto) {
    return this.text2imageService.checkHash(hash);
  }

  @ApiOperation({ summary: 'Очистить очередь' })
  @ApiResponse({ status: 200, type: Boolean })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Get('/clear_queue')
  async clearQueue() {
    return this.text2imageService.clearQueue();
  }

  @ApiOperation({ summary: 'Проверить наличие сущностей по заданному хешу' })
  @ApiResponse({ status: 200, type: CheckQueueResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Get('/:queueType/check_queue')
  async checkQueue(@Param() params: CheckQueueReqDto) {
    return this.text2imageService.checkQueue(params);
  }

  @ApiOperation({ summary: 'Поместить задачу в очередь на обработку' })
  @ApiResponse({ status: 200, type: PushToQueueResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Post('/push')
  async pushToQueue(@Body() body: T2IPushToQueueReqDto) {
    return this.text2imageService.pushToQueue(body);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: BunchStatusResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiNoContentResponse({ description: 'По запросу ничего не найдено' })
  @Get('/:queueType/bunches/:bunchId/status')
  async bunchStatus(@Param() params: T2IBunchStatusReqDto) {
    const result = await this.text2imageService.getBunchStatus(params.bunchId);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }

  @ApiOperation({ summary: 'Получить пакет' })
  @ApiResponse({ status: 200, type: [EntityDto] })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiNoContentResponse({ description: 'По запросу ничего не найдено' })
  @Get('/:queueType/bunches/:bunchId/entities')
  async bunchEntities(@Param() params: T2IBunchStatusReqDto) {
    const result = await this.text2imageService.getBunchEntities(
      params.bunchId,
    );

    if (!result || !result.length) {
      throw new NoContentException();
    }

    return result;
  }

  @ApiOperation({ summary: 'Получение сущности' })
  @ApiResponse({ status: 200, type: EntityDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiNoContentResponse({ description: 'По запросу ничего не найдено' })
  @Get('/:queueType/bunches/:bunchId/entities/:entityId')
  async getBunchEntity(@Param() params: T2IEntityReqDto) {
    const result = await this.text2imageService.getBunchEntity(params.entityId);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }

  @ApiOperation({ summary: 'Получение задачи из очереди' })
  @ApiResponse({ status: 200, type: GetQueueItemResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiNoContentResponse({ description: 'По запросу ничего не найдено' })
  @Post('/get_task')
  async getQueueItem(@Body() body: GetQueueItemReqDto) {
    const result = await this.text2imageService.getQueueItem(body);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }

  @ApiOperation({ summary: 'Запись обработанной задачи из очереди' })
  @ApiResponse({ status: 200, type: Boolean })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @Post('/send_result')
  async setProcessingResult(@Body() params: SetResultReqDto) {
    const result = await this.text2imageService.setProcessedEntity(params);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }
}
