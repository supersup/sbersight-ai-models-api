import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Entity, EntitySchema } from './dao/models/entity.schema';
import { Bunch, BunchSchema } from './dao/models/bunch.schema';
import { Queue, QueueSchema } from './dao/models/queue.schema';
import EntityRepository from './dao/repository/EntityRepository';
import BunchRepository from './dao/repository/BunchRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { StyletransferService } from './styletransfer.service';
import { StyletransferController } from './styletransfer.controller';
import Configuration from 'src/config/Configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [Configuration.configFactory],
    }),
    MongooseModule.forFeature(
      [
        { name: Entity.name, schema: EntitySchema },
        { name: Bunch.name, schema: BunchSchema },
        { name: Queue.name, schema: QueueSchema },
      ],
      process.env.ST_DB_NAME,
    ),
  ],
  controllers: [StyletransferController],
  providers: [
    StyletransferService,
    EntityRepository,
    BunchRepository,
    QueueRepository,
  ],
})
export class StyletransferModule {}
