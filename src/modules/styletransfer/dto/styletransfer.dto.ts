import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt } from 'class-validator';
import { BunchStatusReqDto } from 'src/dto/bunch.dto';
import { EntityReqDto } from 'src/dto/entity.dto';
import { QueueType } from './model.dto';

export class CheckQueueResDto {
  @ApiProperty({
    description: 'Сущностей в очереди',
    type: Number,
  })
  @IsInt()
  count: number;
}

export class T2IBunchStatusReqDto extends BunchStatusReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}

export class T2IEntityReqDto extends EntityReqDto {
  @ApiProperty({
    description: 'Тип очереди',
    enum: QueueType,
  })
  @IsEnum(QueueType)
  queueType: QueueType;
}
