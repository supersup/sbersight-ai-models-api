import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Status } from 'src/dto/status.dto';

export enum ModelTypes {
  CLASSIC = 'classic',
  FAST = 'fast',
}

export interface EntityDtoImpl {
  _id: string;
  status: Status;
  type: ModelTypes;
  defaultStyleImage: string;
  iters: number;
  originalImage: string;
  scaleImage: number;
  styleImage: string;
  superRes: number;
}

export class EntityDto implements EntityDtoImpl {
  @ApiProperty({
    description: 'Уникальный идентификатор сущности',
    required: true,
  })
  _id: string;

  @ApiProperty({
    description: 'Указатель целевой модели',
    enum: Status,
    required: true,
  })
  status: Status;

  @ApiProperty({
    description: 'Указатель целевой модели',
    enum: ModelTypes,
    required: true,
  })
  type: ModelTypes;

  @ApiProperty({
    description: 'Название дефолтного стиля для модели',
    required: true,
  })
  defaultStyleImage: string;

  @ApiProperty({
    description: 'Кол-во итераций обработки картинки',
    required: true,
  })
  iters: number;

  @ApiProperty({
    description:
      'content_img_base64 (передача картинки для обработка, закодированной в base64)',
    required: true,
  })
  originalImage: string;

  @ApiProperty({
    description:
      'Параметр для возможного сжатия обработанной картинки пользователя Возможные значения: 0 <= 1',
    required: true,
  })
  scaleImage: number;

  @ApiProperty({
    description:
      'style_img_base64 (передача пользовательской картинки со стилем)',
    required: true,
  })
  styleImage: string;

  @ApiProperty({
    description:
      'Переменная, которая отвечает за увеличение размера изображения при обработке (за подключение модели super Resolution) Возможные значения: 1, 2, 4',
    required: true,
  })
  superRes: number;
}

export class STPushToQueueItemsDto extends OmitType(EntityDto, [
  '_id',
  'status',
] as const) {}

export class STPushToQueueReqDto {
  @ApiProperty({
    description: 'Массив изображений',
    type: [STPushToQueueItemsDto],
    required: true,
  })
  items: STPushToQueueItemsDto[];
}

export interface PushToQueueResImpl {
  bunchId: string;
}

export class PushToQueueDto implements PushToQueueResImpl {
  @ApiProperty({ description: 'Уникальный идентификатор пакета' })
  bunchId: string;
}
