import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { StyletransferController } from './styletransfer.controller';
import { StyletransferService } from './styletransfer.service';
import BunchRepository from './dao/repository/BunchRepository';
import EntityRepository from './dao/repository/EntityRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { ModelTypes } from './dto/entity.dto';

describe('StyletransferController', () => {
  let styletransferController: StyletransferController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [StyletransferController],
      providers: [
        StyletransferService,
        BunchRepository,
        EntityRepository,
        QueueRepository,
      ],
    }).compile();

    styletransferController = app.get<StyletransferController>(
      StyletransferController,
    );
  });

  describe('root', () => {
    it('should return bunch id', () => {
      const target = styletransferController.pushToQueue({
        items: [
          {
            type: ModelTypes.CLASSIC,
            iters: 1,
            defaultStyleImage: 'temp',
            styleImage: 'temp',
            superRes: 1,
            scaleImage: 1,
            originalImage: 'temp',
          },
        ],
      });

      expect(target).toContain({ bunchId: '6197b57e49ad29f8d0e2c184' });
    });
  });
});
