import { LeanDocument } from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import got from 'src/util/got';
import { Config } from 'src/config/Configuration';
import { STPushToQueueReqDto } from './dto/entity.dto';
import { ModelResDto } from './dto/model.dto';
import BunchRepository from './dao/repository/BunchRepository';
import EntityRepository from './dao/repository/EntityRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { EntityDocument } from './dao/models/entity.schema';
import { Status } from 'src/dto/status.dto';
import {
  GetQueueItemReqDto,
  SetResultReqDto,
  TaskStatus,
} from './dto/model.dto';
import { NoContentException } from 'src/exceptions';

@Injectable()
export class StyletransferService {
  constructor(
    private configService: ConfigService<Config>,
    private bunchRepository: BunchRepository,
    private entityRepository: EntityRepository,
    private queueRepository: QueueRepository,
  ) {}

  async clearQueue() {
    const queueItems = await this.queueRepository.find({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      status: {
        $in: [Status.INITIAL],
      },
    });

    queueItems.forEach((queue) => {
      this.bunchRepository.findById(queue.bunchId).then((bunch) => {
        this.entityRepository
          .find({ bunchId: bunch._id })
          .then((items) => items.forEach((i) => i.delete()));
        bunch.delete();
      });

      queue.delete();
    });

    return true;
  }

  async checkQueue() {
    const queueCount = await this.queueRepository.count({
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      status: Status.INITIAL,
    });

    return { count: queueCount };
  }

  async pushToQueue({ items }: STPushToQueueReqDto) {
    try {
      const bunch = await this.bunchRepository.create();

      items.map((item) => {
        this.entityRepository.create({
          bunchId: bunch._id,
          params: item,
          type: item.type,
        });
      });

      this.queueRepository.create({
        bunchId: bunch._id,
      });

      return {
        bunchId: bunch._id,
      };
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getBunchStatus(bunchId: string) {
    const bunch = await this.bunchRepository.findById(bunchId);

    // TODO: return status;
    // return bunch.status;
    return bunch;
  }

  async getBunchEntities(bunchId: string) {
    const entities = await this.entityRepository.find({ bunchId });

    return entities;
  }

  async getBunchEntity(entityId: string) {
    const entity = await this.entityRepository.findById(entityId);

    return entity;
  }

  async processEntity(entity: LeanDocument<EntityDocument>) {
    try {
      const aiModelUrl = this.configService.get(
        `styletransfer.aiModelHosts.${entity.type}`,
        { infer: true },
      );

      const result = await got
        .post(aiModelUrl, {
          json: {
            ...entity,
            saveColors: false,
          },
        })
        .json<ModelResDto>();

      if (result?.resultImage) {
        this.entityRepository.update(entity._id, {
          result: result,
          status: Status.SUCCESS,
        });
      } else {
        this.entityRepository.update(entity._id, {
          error: result,
          status: Status.FAIL,
        });
      }
    } catch (e) {
      this.entityRepository.update(entity._id, {
        error: e.message,
        status: Status.FAIL,
      });
    }
  }

  // TODO: Now another workflow works
  // @Cron(CronExpression.EVERY_SECOND)
  async processQueue() {
    const isQueueFilled = Boolean(await this.queueRepository.count());
    const queues = await this.queueRepository.findAll();
    const initialQueues = [];
    const hasInitialQueues = queues.some((i) => {
      if (i.status === 'INITIAL') {
        initialQueues.push(i);

        return true;
      }
    });

    if (isQueueFilled && hasInitialQueues) {
      const { bunchId, _id: queueId } = initialQueues[0];
      this.queueRepository.update(queueId, {
        status: 'PROCESSING',
      });
      const entities = await this.entityRepository.find({
        bunchId: bunchId,
      });

      this.bunchRepository.update(bunchId, { status: Status.PROCESSING });

      await Promise.all(
        entities.map((entity) => this.processEntity(entity.toObject())),
      );

      this.bunchRepository.update(bunchId, { status: Status.SUCCESS });
      this.queueRepository.delete(queueId);
    }
  }

  async getQueueItem(params: GetQueueItemReqDto) {
    const freshItem = await this.queueRepository.findOneAndUpdate(
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: Status.INITIAL,
      },
      {
        status: Status.PROCESSING,
      },
    );

    if (freshItem) {
      const bunchId = freshItem?.bunchId;
      const queueId = freshItem?._id;
      const entities = await this.entityRepository.find({
        bunchId: bunchId,
      });

      if (!entities.length) {
        throw new NoContentException();
      }

      const entity = entities[0];

      if (entity) {
        this.queueRepository.update(queueId, {
          status: Status.PROCESSING,
          updatedAt: Date.now(),
        });
        this.bunchRepository.update(bunchId, {
          status: Status.PROCESSING,
          updatedAt: Date.now(),
        });
        this.entityRepository.update(entity._id, {
          status: Status.PROCESSING,
          updatedAt: Date.now(),
        });

        return {
          query_id: queueId,
          task: entity.params.defaultStyleImage
            ? TaskStatus.FAST
            : TaskStatus.CLASSIC,
          ...entity.params,
        };
      }
    }

    return {
      task: TaskStatus.NONE,
    };
  }

  async setProcessedEntity(params: SetResultReqDto) {
    try {
      const queueItem = await this.queueRepository.findById(params.query_id);

      if (!queueItem) {
        return null;
      }

      const bunch = await this.bunchRepository.findById(queueItem.bunchId);
      const entities = await this.entityRepository.find({
        bunchId: bunch._id,
      });
      const entity = entities[0];

      if (params.generated_images.length) {
        this.entityRepository.update(entity._id, {
          response: params.generated_images,
          result: { resultImage: params.generated_images[0] },
          status: Status.SUCCESS,
          updatedAt: Date.now(),
        });
        this.bunchRepository.update(bunch._id, {
          status: Status.SUCCESS,
          updatedAt: Date.now(),
        });
        this.queueRepository.delete(params.query_id);
      } else {
        this.entityRepository.update(entity._id, {
          error: params,
          status: Status.FAIL,
        });
      }

      return true;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async pruneProcessingItems() {
    const FIVE_MINUTE = 300_000;

    const expiredQueueItems = await this.queueRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      updatedAt: { $lte: new Date(Date.now() - FIVE_MINUTE) },
      //  FIXME: fix types of filter
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      status: {
        $in: [Status.PROCESSING],
      },
    });

    expiredQueueItems.forEach((item) => {
      this.bunchRepository.findById(item.bunchId).then((i) => {
        this.bunchRepository.update(i._id, {
          status: Status.SUCCESS,
          updatedAt: Date.now(),
        });
      });

      this.entityRepository.find({ bunchId: item.bunchId }).then((items) => {
        items.forEach((i) => {
          this.entityRepository.update(i._id, {
            error: 'Is too long in proccess',
            status: Status.FAIL,
            updatedAt: Date.now(),
          });
        });
      });

      item.delete();
    });
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async pruneDatabase() {
    const FOUR_HOUR = 14_400_000;
    const DAY = 86_400_000_000;
    const orCondition = [
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        updatedAt: { $lte: new Date(Date.now() - FOUR_HOUR) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createdAt: { $lte: new Date(Date.now() - DAY) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
    ];
    const expiredQueueItems = await this.queueRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
    const expiredBunches = await this.bunchRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });

    expiredQueueItems.forEach((item) => item.delete());
    expiredBunches.forEach((bunch) => {
      this.entityRepository
        .find({ bunchId: bunch._id })
        .then((items) => items.forEach((i) => i.delete()));
      bunch.delete();
    });
  }
}
