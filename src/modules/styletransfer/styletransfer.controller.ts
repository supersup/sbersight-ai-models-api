import { NoContentException } from 'src/exceptions/NoContentException';
import { Body, Controller, HttpStatus, Post, Get, Param } from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';
import { StyletransferService } from './styletransfer.service';
import {
  STPushToQueueReqDto,
  PushToQueueDto,
  EntityDto,
} from './dto/entity.dto';
import { BunchStatusReqDto, BunchStatusResDto } from './dto/bunch.dto';
import {
  GetQueueItemResDto,
  GetQueueItemReqDto,
  SetResultReqDto,
} from './dto/model.dto';
import { CheckQueueResDto } from './dto/styletransfer.dto';

@ApiTags('styletransfer')
@Controller('styletransfer')
export class StyletransferController {
  constructor(private styletransferService: StyletransferService) {}

  @ApiOperation({ summary: 'Очистить очередь' })
  @ApiResponse({ status: 200, type: Boolean })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Get('/clear_queue')
  async clearQueue() {
    return this.styletransferService.clearQueue();
  }

  @ApiOperation({ summary: 'Проверить наличие сущностей по заданному хешу' })
  @ApiResponse({ status: 200, type: CheckQueueResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @Get('/check_queue')
  async checkQueue() {
    return this.styletransferService.checkQueue();
  }

  @ApiOperation({ summary: 'Поместить задачу в очередь на обработку' })
  @ApiResponse({ status: 200, type: PushToQueueDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Post('/push')
  async pushToQueue(@Body() body: STPushToQueueReqDto) {
    return this.styletransferService.pushToQueue(body);
  }

  @ApiOperation({ summary: 'Статус по пакету' })
  @ApiResponse({ status: 200, type: BunchStatusResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/status')
  async getBunchStatus(@Param() params: BunchStatusReqDto) {
    const status = await this.styletransferService.getBunchStatus(
      params.bunchId,
    );

    return status;
  }

  @ApiOperation({ summary: 'Данные по пакету' })
  @ApiResponse({ status: 200, type: [EntityDto] })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities')
  async getBunchEntities(@Param() params: BunchStatusReqDto) {
    const result = await this.styletransferService.getBunchEntities(
      params.bunchId,
    );

    return result;
  }

  @ApiOperation({ summary: 'Получение сущности' })
  @ApiResponse({ status: 200, type: EntityDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities/:entityId')
  async getBunchEntity(@Param() params: BunchStatusReqDto) {
    return this.styletransferService.getBunchEntity(params.entityId);
  }

  @ApiOperation({ summary: 'Получение задачи из очереди' })
  @ApiResponse({ status: 200, type: GetQueueItemResDto })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiNoContentResponse({ description: 'По запросу ничего не найдено' })
  @Post('/get_task')
  async getQueueItem(@Body() body: GetQueueItemReqDto) {
    const result = await this.styletransferService.getQueueItem(body);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }

  @ApiOperation({ summary: 'Запись обработанной задачи из очереди' })
  @ApiResponse({ status: 200, type: Boolean })
  @ApiInternalServerErrorResponse({
    description: 'Произошла неизвестная ошибка',
  })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @Post('/send_result')
  async setProcessingResult(@Body() params: SetResultReqDto) {
    const result = await this.styletransferService.setProcessedEntity(params);

    if (!result) {
      throw new NoContentException();
    }

    return result;
  }
}
