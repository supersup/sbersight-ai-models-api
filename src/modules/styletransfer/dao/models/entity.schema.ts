import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsDefined } from 'class-validator';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export enum ModelTypes {
  CLASSIC = 'classic',
  FAST = 'fast',
}

export interface EntityDocument extends Document, Entity {}

@Schema()
export class Entity {
  @Prop({ required: true })
  bunchId: string;

  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: Date;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  updatedAt?: Date;

  @Prop({ type: MongooseSchema.Types.Mixed })
  result?: any;

  @Prop()
  error?: string;

  @Prop({ enum: ModelTypes, required: true })
  type: ModelTypes;

  @Prop({ type: [MongooseSchema.Types.Mixed], default: null })
  response?: any[];

  @Prop({ type: MongooseSchema.Types.Mixed, default: null })
  params: any;
}

export const EntitySchema = SchemaFactory.createForClass(Entity);
