import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Status } from 'src/dto/status.dto';

export interface BunchDocument extends Document, Bunch {}

@Schema()
export class Bunch {
  @Prop({ enum: Status, default: Status.INITIAL })
  status?: Status;

  @Prop({ type: MongooseSchema.Types.Date, default: Date.now })
  createdAt?: Date;
}

export const BunchSchema = SchemaFactory.createForClass(Bunch);
