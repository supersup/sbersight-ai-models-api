import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, UpdateQuery, FilterQuery } from 'mongoose';
import { Bunch, BunchDocument } from '../models/bunch.schema';

@Injectable()
class BunchRepository {
  constructor(
    @InjectModel(Bunch.name) private bunchModel: Model<BunchDocument>,
  ) {}

  create(queue: Bunch = {}) {
    const createdBunch = new this.bunchModel(queue);

    return createdBunch.save();
  }

  delete(id: string) {
    return this.bunchModel.findByIdAndDelete(id).exec();
  }

  update(id: string, update: UpdateQuery<typeof Bunch>) {
    return this.bunchModel
      .findByIdAndUpdate(id, update, { useFindAndModify: false })
      .exec();
  }

  findAll() {
    return this.bunchModel.find({}).exec();
  }

  find(filter: FilterQuery<BunchDocument>, projection?: any) {
    return this.bunchModel.find(filter, projection).exec();
  }

  findById(id: string, projection?: any) {
    return this.bunchModel.findById(id, projection).exec();
  }

  findOne(filter: Partial<Bunch>, projection?: any) {
    return this.bunchModel.findOne(filter, projection).exec();
  }

  deleteMany(filter: Partial<Bunch>) {
    return this.bunchModel.deleteMany(filter).exec();
  }
}

export default BunchRepository;
