import { ApiProperty, PickType } from '@nestjs/swagger';
import { Status } from 'src/dto/status.dto';

export interface EntityDtoImpl {
  _id: string;
  status: Status;
  image: string;
}

export class EntityDto implements EntityDtoImpl {
  @ApiProperty({
    description: 'Уникальный идентификатор сущности',
    required: true,
  })
  _id: string;

  @ApiProperty({
    description: 'Статус',
    enum: Status,
    required: true,
  })
  status: Status;

  @ApiProperty({
    description: 'Название дефолтного стиля для модели',
    required: true,
  })
  image: string;

  @ApiProperty({ description: 'Название файла' })
  filename: string;
}

export class DPPushToQueueItemsDto extends PickType(EntityDto, [
  'image',
  'filename',
] as const) {}

export class DPPushToQueueReqDto {
  @ApiProperty({
    description: 'Массив изображений',
    type: [DPPushToQueueItemsDto],
    required: true,
  })
  items: DPPushToQueueItemsDto[];
}

export interface PushToQueueResImpl {
  bunchId: string;
}

export class PushToQueueResDto implements PushToQueueResImpl {
  @ApiProperty({ description: 'Уникальный идентификатор пакета' })
  bunchId: string;
}
