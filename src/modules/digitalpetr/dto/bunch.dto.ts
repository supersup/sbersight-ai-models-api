import { ApiProperty, PickType } from '@nestjs/swagger';
import { Status } from 'src/dto/status.dto';

export interface BunchImpl {
  _id: string;
  status: string;
  createdAt: Date;
}

export class BunchDto implements BunchImpl {
  @ApiProperty({ description: 'Id пакета' })
  _id: string;

  @ApiProperty({ description: 'Статус' })
  status: Status;

  @ApiProperty({ description: 'Дата создания' })
  createdAt: Date;
}

interface BunchStatusReqDtoImpl {
  bunchId: string;
  entityId: string;
}

export class BunchStatusReqDto implements BunchStatusReqDtoImpl {
  @ApiProperty({ description: 'Id пакета' })
  bunchId: string;

  @ApiProperty({ description: 'Id пакета' })
  entityId: string;
}

export class BunchStatusResDto extends PickType(BunchDto, [
  'status',
] as const) {}
