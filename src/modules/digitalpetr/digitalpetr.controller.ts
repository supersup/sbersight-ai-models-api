import { Body, Controller, HttpStatus, Post, Get, Param } from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiUnauthorizedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DigitalpetrService } from './digitalpetr.service';
import {
  EntityDto,
  DPPushToQueueReqDto,
  PushToQueueResDto,
} from './dto/entity.dto';
import { BunchStatusReqDto, BunchStatusResDto } from './dto/bunch.dto';

@ApiTags('digital-petr')
@Controller('digital-petr')
export class DigitalpetrController {
  constructor(private digitalpetrService: DigitalpetrService) {}

  @ApiOperation({ summary: 'Поместить задачу в очередь на обработку' })
  @ApiResponse({ status: 200, type: PushToQueueResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Post('/push')
  async pushToQueue(@Body() body: DPPushToQueueReqDto) {
    return this.digitalpetrService.pushToQueue(body);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: BunchStatusResDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/status')
  async bunchStatus(@Param() params: BunchStatusReqDto) {
    return this.digitalpetrService.getBunchStatus(params.bunchId);
  }

  @ApiOperation({ summary: 'Получить статус по пакету' })
  @ApiResponse({ status: 200, type: [EntityDto] })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities')
  async bunchEntities(@Param() params: BunchStatusReqDto) {
    return this.digitalpetrService.getBunchEntities(params.bunchId);
  }

  @ApiOperation({ summary: 'Получение сущности' })
  @ApiResponse({ status: 200, type: EntityDto })
  @ApiForbiddenResponse({
    status: HttpStatus.FORBIDDEN,
    description: 'Нет доступа',
  })
  @ApiUnauthorizedResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Ошибка авторизации',
  })
  @Get('/bunches/:bunchId/entities/:entityId')
  async getBunchEntity(@Param() params: BunchStatusReqDto) {
    return this.digitalpetrService.getBunchEntity(params.entityId);
  }
}
