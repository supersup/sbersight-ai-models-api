import { Test, TestingModule } from '@nestjs/testing';
import { DigitalpetrController } from './digitalpetr.controller';
import { DigitalpetrService } from './digitalpetr.service';

describe('FaceswapController', () => {
  let digitalpetrController: DigitalpetrController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DigitalpetrController],
      providers: [DigitalpetrService],
    }).compile();

    digitalpetrController = app.get<DigitalpetrController>(
      DigitalpetrController,
    );
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const target = digitalpetrController.pushToQueue({
        items: [
          {
            image: 'asdgasdgas',
            filename: 'asdgasdg',
          },
        ],
      });

      expect(target).toContain({ bunchId: '6197b57e49ad29f8d0e2c184' });
    });
  });
});
