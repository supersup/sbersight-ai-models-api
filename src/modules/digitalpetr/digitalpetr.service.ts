import { Cron, CronExpression } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import got, { getHooks } from 'src/util/got';
import { Config } from 'src/config/Configuration';
import { DPPushToQueueReqDto } from './dto/entity.dto';
import { ModelResDto } from './dto/aiModel.dto';
import BunchRepository from './dao/repository/BunchRepository';
import EntityRepository from './dao/repository/EntityRepository';
import QueueRepository from './dao/repository/QueueRepository';
import { EntityDocument } from './dao/models/entity.schema';
import { Status } from 'src/dto/status.dto';

@Injectable()
export class DigitalpetrService {
  constructor(
    private configService: ConfigService<Config>,
    private bunchRepository: BunchRepository,
    private entityRepository: EntityRepository,
    private queueRepository: QueueRepository,
  ) {}

  async pushToQueue(data: DPPushToQueueReqDto) {
    try {
      const bunch = await this.bunchRepository.create();

      data.items.map((item) => {
        this.entityRepository.create({
          bunchId: bunch._id,
          image: item.image,
          filename: item.filename,
        });
      });

      this.queueRepository.create({
        bunchId: bunch._id,
      });

      return {
        bunchId: bunch._id,
      };
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getBunchStatus(bunchId: string) {
    return this.bunchRepository.findById(bunchId, { status: true });
  }

  async getBunchEntities(bunchId: string) {
    return this.entityRepository.find({ bunchId });
  }

  async getBunchEntity(entityId: string) {
    return this.entityRepository.findById(entityId);
  }

  async processEntity(entity: EntityDocument) {
    try {
      const aiModelUrl = this.configService.get('digitalpetr.aiModelHost', {
        infer: true,
      });
      const deployId = this.configService.get('digitalpetr.deployId', {
        infer: true,
      });

      const xWorkspaceId = this.configService.get('digitalpetr.xWorkspaceId', {
        infer: true,
      });

      const result = await got
        .post(aiModelUrl.replace(/\{\w+\}/g, deployId), {
          headers: {
            'x-workspace-id': xWorkspaceId,
          },
          json: {
            key: 'c00504e7bdad49518179d1887bb076c0',
            image: entity.image,
          },
        })
        .json<ModelResDto>();

      if (result?.predictions && result.predictions !== 'No text detected') {
        this.entityRepository.update(entity._id, {
          result: result.predictions,
          status: Status.SUCCESS,
        });
      } else {
        this.entityRepository.update(entity._id, {
          error: result.predictions,
          status: Status.FAIL,
        });
      }
    } catch (e) {
      console.error(e);
      this.entityRepository.update(entity._id, {
        error: e.message,
        status: Status.FAIL,
      });
    }
  }

  @Cron(CronExpression.EVERY_SECOND)
  async processQueue() {
    const isQueueFilled = Boolean(await this.queueRepository.count());
    const queues = await this.queueRepository.findAll();
    const initialQueues = [];
    const hasInitialQueues = queues.some((i) => {
      if (i.status === 'INITIAL') {
        initialQueues.push(i);

        return true;
      }
    });

    if (isQueueFilled && hasInitialQueues) {
      const { bunchId, _id: queueId } = initialQueues[0];
      this.queueRepository.update(queueId, {
        status: 'PROCESSING',
      });
      const entities = await this.entityRepository.find({
        bunchId: bunchId,
      });

      this.bunchRepository.update(bunchId, { status: Status.PROCESSING });

      await Promise.all(
        entities.map(async (entity) => this.processEntity(entity)),
      );

      this.bunchRepository.update(bunchId, { status: Status.SUCCESS });
      this.queueRepository.delete(queueId);
    }
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async pruneDatabase() {
    const DAY = 86_400_000_000;
    const orCondition = [
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        updatedAt: { $lte: new Date(Date.now() - DAY) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
      {
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        createdAt: { $lte: new Date(Date.now() - DAY) },
        //  FIXME: fix types of filter
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        status: {
          $in: [Status.INITIAL, Status.PROCESSING, Status.SUCCESS, Status.FAIL],
        },
      },
    ];
    const expiredQueueItems = await this.queueRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });
    const expiredBunches = await this.bunchRepository.find({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      $or: orCondition,
    });

    expiredQueueItems.forEach((item) => item.delete());
    expiredBunches.forEach((bunch) => {
      this.entityRepository
        .find({ bunchId: bunch._id })
        .then((items) => items.forEach((i) => i.delete()));
      bunch.delete();
    });
  }
}
