import { Controller, Get } from '@nestjs/common';
import {
  HealthCheckService,
  HttpHealthIndicator,
  HealthCheck,
} from '@nestjs/terminus';
import { AxiosRequestConfig } from '@nestjs/terminus/dist/health-indicator/http/axios.interfaces';

@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private http: HttpHealthIndicator,
  ) {}

  async validateResponse(
    key: string,
    endpoint: string,
    options?: AxiosRequestConfig,
  ) {
    try {
      // const res = await this.http.pingCheck(key, endpoint, options);
      const res = await this.http.responseCheck(
        key,
        endpoint,
        (res) => {
          const httpSuccessStatuses = [200, 400, 422];

          return httpSuccessStatuses.includes(res.status);
        },
        options,
      );

      return res;
    } catch (error) {
      console.error(error);
    }
  }

  @Get()
  @HealthCheck()
  check() {
    return this.health.check([
      // DIGITAL PETER

      () =>
        this.validateResponse(
          'AI_MODELS_API',
          `${process.env.HOST}/api/v1/models/swagger`,
          // {
          //   method: 'POST',
          // },
        ),
    ]);
  }
}
